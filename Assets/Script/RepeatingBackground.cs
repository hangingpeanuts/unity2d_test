﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RepeatingBackground : MonoBehaviour {

    private BoxCollider2D groundCollider;
    private float groundHorizontalLength;
    private SpriteRenderer sr;
    private bool isPositioning = false;
    
    

	// Use this for initialization
	void Start () {
        sr = GetComponent<SpriteRenderer>();
        groundCollider = GetComponent<BoxCollider2D>();
        groundHorizontalLength = sr.size.x;
       // Debug.Log("image size " + groundHorizontalLength);
    }
	
	// Update is called once per frame
	void Update () {
        Debug.Log("image size " + groundHorizontalLength);
        Debug.Log("position " + transform.position.x);
        if (transform.position.x < -groundHorizontalLength && isPositioning == false)
        {
            isPositioning = true;
            RepositionBackground();            
        }
	}

    private void RepositionBackground()
    {
        Vector2 groundOffset = new Vector2(groundHorizontalLength * 2f, 0);
        transform.position = (Vector2)transform.position + groundOffset;
        isPositioning = false;
    }
}
